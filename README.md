# Angular backend/frontend exam.


- **Test 1:** Set up the entire front-end and backend on your local. 
- **Test 2:** Add the following functionality:
For each resident or business registration, generate a QRCode.
    - QRCode should be displayed upon saving.
    - QRCode formation for resident should include: firstname, lastname and assigned database identifier
    - QRCode formation for business should include: business number, contact name and assigned database identifier
- **Test 3:** Deploy the application on heroku or any cloud services with free tier (domain name is not required).

Once done, set up a calendar invite with me to discuss your code.

https://calendly.com/areyesonl/15min




