import MongooseRepository from './mongooseRepository';
import MongooseQueryUtils from '../utils/mongooseQueryUtils';
import AuditLogRepository from './auditLogRepository';
import Error404 from '../../errors/Error404';
import { IRepositoryOptions } from './IRepositoryOptions';
import Resident from '../models/resident';

class ResidentRepository {
  
  static async create(data, options: IRepositoryOptions) {
    const currentTenant = MongooseRepository.getCurrentTenant(
      options,
    );

    const currentUser = MongooseRepository.getCurrentUser(
      options,
    );

    const [record] = await Resident(
      options.database,
    ).create(
      [
        {
          ...data,
          tenant: currentTenant.id,
          createdBy: currentUser.id,
          updatedBy: currentUser.id,
        },
      ],
      MongooseRepository.getSessionOptionsIfExists(options),
    );

    await this._createAuditLog(
      AuditLogRepository.CREATE,
      record.id,
      data,
      options,
    );

    

    return this.findById(record.id, options);
  }

  static async update(id, data, options: IRepositoryOptions) {
    const currentTenant = MongooseRepository.getCurrentTenant(
      options,
    );

    let record = await MongooseRepository.wrapWithSessionIfExists(
      Resident(options.database).findById(id),
      options,
    );

    if (
      !record ||
      String(record.tenant) !== String(currentTenant.id)
    ) {
      throw new Error404();
    }

    await MongooseRepository.wrapWithSessionIfExists(
      Resident(options.database).updateOne(
        { _id: id },
        {
          ...data,
          updatedBy: MongooseRepository.getCurrentUser(
            options,
          ).id,
        },
      ),
      options,
    );

    await this._createAuditLog(
      AuditLogRepository.UPDATE,
      id,
      data,
      options,
    );

    record = await this.findById(id, options);



    return record;
  }

  static async destroy(id, options: IRepositoryOptions) {
    const currentTenant = MongooseRepository.getCurrentTenant(
      options,
    );

    let record = await MongooseRepository.wrapWithSessionIfExists(
      Resident(options.database).findById(id),
      options,
    );

    if (
      !record ||
      String(record.tenant) !== String(currentTenant.id)
    ) {
      throw new Error404();
    }

    await MongooseRepository.wrapWithSessionIfExists(
      Resident(options.database).deleteOne({ _id: id }),
      options,
    );

    await this._createAuditLog(
      AuditLogRepository.DELETE,
      id,
      record,
      options,
    );


  }

  static async count(filter, options: IRepositoryOptions) {
    const currentTenant = MongooseRepository.getCurrentTenant(
      options,
    );

    return MongooseRepository.wrapWithSessionIfExists(
      Resident(options.database).countDocuments({
        ...filter,
        tenant: currentTenant.id,
      }),
      options,
    );
  }

  static async findById(id, options: IRepositoryOptions) {
    const currentTenant = MongooseRepository.getCurrentTenant(
      options,
    );

    let record = await MongooseRepository.wrapWithSessionIfExists(
      Resident(options.database)
        .findById(id),
      options,
    );

    if (
      !record ||
      String(record.tenant) !== String(currentTenant.id)
    ) {
      throw new Error404();
    }

    return this._fillFileDownloadUrls(record);
  }

  static async findAndCountAll(
    { filter, limit = 0, offset = 0, orderBy = '' },
    options: IRepositoryOptions,
  ) {
    const currentTenant = MongooseRepository.getCurrentTenant(
      options,
    );

    let criteriaAnd: any = [];
    
    criteriaAnd.push({
      tenant: currentTenant.id,
    });

    if (filter) {
      if (filter.id) {
        criteriaAnd.push({
          ['_id']: MongooseQueryUtils.uuid(filter.id),
        });
      }

      if (filter.suffix) {
        criteriaAnd.push({
          suffix: {
            $regex: MongooseQueryUtils.escapeRegExp(
              filter.suffix,
            ),
            $options: 'i',
          },
        });
      }

      if (filter.firstName) {
        criteriaAnd.push({
          firstName: {
            $regex: MongooseQueryUtils.escapeRegExp(
              filter.firstName,
            ),
            $options: 'i',
          },
        });
      }

      if (filter.middleName) {
        criteriaAnd.push({
          middleName: {
            $regex: MongooseQueryUtils.escapeRegExp(
              filter.middleName,
            ),
            $options: 'i',
          },
        });
      }

      if (filter.lastName) {
        criteriaAnd.push({
          lastName: {
            $regex: MongooseQueryUtils.escapeRegExp(
              filter.lastName,
            ),
            $options: 'i',
          },
        });
      }

      if (
        filter.planToVaccinate === true ||
        filter.planToVaccinate === 'true' ||
        filter.planToVaccinate === false ||
        filter.planToVaccinate === 'false'
      ) {
        criteriaAnd.push({
          planToVaccinate:
            filter.planToVaccinate === true ||
            filter.planToVaccinate === 'true',
        });
      }

      if (filter.birthDateRange) {
        const [start, end] = filter.birthDateRange;

        if (start !== undefined && start !== null && start !== '') {
          criteriaAnd.push({
            birthDate: {
              $gte: start,
            },
          });
        }

        if (end !== undefined && end !== null && end !== '') {
          criteriaAnd.push({
            birthDate: {
              $lte: end,
            },
          });
        }
      }

      if (filter.region) {
        criteriaAnd.push({
          region: {
            $regex: MongooseQueryUtils.escapeRegExp(
              filter.region,
            ),
            $options: 'i',
          },
        });
      }

      if (filter.city) {
        criteriaAnd.push({
          city: {
            $regex: MongooseQueryUtils.escapeRegExp(
              filter.city,
            ),
            $options: 'i',
          },
        });
      }

      if (filter.barangay) {
        criteriaAnd.push({
          barangay: {
            $regex: MongooseQueryUtils.escapeRegExp(
              filter.barangay,
            ),
            $options: 'i',
          },
        });
      }

      if (filter.email) {
        criteriaAnd.push({
          email: {
            $regex: MongooseQueryUtils.escapeRegExp(
              filter.email,
            ),
            $options: 'i',
          },
        });
      }

      if (filter.mobileNumber) {
        criteriaAnd.push({
          mobileNumber: {
            $regex: MongooseQueryUtils.escapeRegExp(
              filter.mobileNumber,
            ),
            $options: 'i',
          },
        });
      }

      if (filter.landline) {
        criteriaAnd.push({
          landline: {
            $regex: MongooseQueryUtils.escapeRegExp(
              filter.landline,
            ),
            $options: 'i',
          },
        });
      }

      if (filter.username) {
        criteriaAnd.push({
          username: {
            $regex: MongooseQueryUtils.escapeRegExp(
              filter.username,
            ),
            $options: 'i',
          },
        });
      }

      if (filter.password) {
        criteriaAnd.push({
          password: {
            $regex: MongooseQueryUtils.escapeRegExp(
              filter.password,
            ),
            $options: 'i',
          },
        });
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (
          start !== undefined &&
          start !== null &&
          start !== ''
        ) {
          criteriaAnd.push({
            ['createdAt']: {
              $gte: start,
            },
          });
        }

        if (
          end !== undefined &&
          end !== null &&
          end !== ''
        ) {
          criteriaAnd.push({
            ['createdAt']: {
              $lte: end,
            },
          });
        }
      }
    }

    const sort = MongooseQueryUtils.sort(
      orderBy || 'createdAt_DESC',
    );

    const skip = Number(offset || 0) || undefined;
    const limitEscaped = Number(limit || 0) || undefined;
    const criteria = criteriaAnd.length
      ? { $and: criteriaAnd }
      : null;

    let rows = await Resident(options.database)
      .find(criteria)
      .skip(skip)
      .limit(limitEscaped)
      .sort(sort);

    const count = await Resident(
      options.database,
    ).countDocuments(criteria);

    rows = await Promise.all(
      rows.map(this._fillFileDownloadUrls),
    );

    return { rows, count };
  }

  static async findAllAutocomplete(search, limit, options: IRepositoryOptions) {
    const currentTenant = MongooseRepository.getCurrentTenant(
      options,
    );

    let criteriaAnd: Array<any> = [{
      tenant: currentTenant.id,
    }];

    if (search) {
      criteriaAnd.push({
        $or: [
          {
            _id: MongooseQueryUtils.uuid(search),
          },
          
        ],
      });
    }

    const sort = MongooseQueryUtils.sort('id_ASC');
    const limitEscaped = Number(limit || 0) || undefined;

    const criteria = { $and: criteriaAnd };

    const records = await Resident(options.database)
      .find(criteria)
      .limit(limitEscaped)
      .sort(sort);

    return records.map((record) => ({
      id: record.id,
      label: record.id,
    }));
  }

  static async _createAuditLog(action, id, data, options: IRepositoryOptions) {
    await AuditLogRepository.log(
      {
        entityName: Resident(options.database).modelName,
        entityId: id,
        action,
        values: data,
      },
      options,
    );
  }

  static async _fillFileDownloadUrls(record) {
    if (!record) {
      return null;
    }

    const output = record.toObject
      ? record.toObject()
      : record;



    return output;
  }
}

export default ResidentRepository;
