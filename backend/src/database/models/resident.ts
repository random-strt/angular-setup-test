import mongoose from 'mongoose';
const Schema = mongoose.Schema;

export default (database) => {
  try {
    return database.model('resident');
  } catch (error) {
    // continue, because model doesnt exist
  }

  const ResidentSchema = new Schema(
    {
      suffix: {
        type: String,
      },
      firstName: {
        type: String,
      },
      middleName: {
        type: String,
      },
      lastName: {
        type: String,
      },
      planToVaccinate: {
        type: Boolean,
        default: false
      },
      birthDate: {
        type: String,
      },
      region: {
        type: String,
      },
      city: {
        type: String,
      },
      barangay: {
        type: String,
      },
      email: {
        type: String,
      },
      mobileNumber: {
        type: String,
      },
      landline: {
        type: String,
      },
      username: {
        type: String,
      },
      password: {
        type: String,
      },
      tenant: {
        type: Schema.Types.ObjectId,
        ref: 'tenant',
      },
      createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'user',
      },
      updatedBy: {
        type: Schema.Types.ObjectId,
        ref: 'user',
      },
      importHash: { type: String },
    },
    { timestamps: true },
  );

  ResidentSchema.index(
    { importHash: 1, tenant: 1 },
    {
      unique: true,
      partialFilterExpression: {
        importHash: { $type: 'string' },
      },
    },
  );

  

  ResidentSchema.virtual('id').get(function () {
    // @ts-ignore
    return this._id.toHexString();
  });

  ResidentSchema.set('toJSON', {
    getters: true,
  });

  ResidentSchema.set('toObject', {
    getters: true,
  });

  return database.model('resident', ResidentSchema);
};
