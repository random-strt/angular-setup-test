import { Component, Input } from '@angular/core';
import { ResidentFormModalService } from 'src/app/resident/form/resident-form-modal.service';
import { ResidentService } from 'src/app/resident/resident.service';

@Component({
  selector: 'app-resident-form-field',
  templateUrl: './resident-form-field.component.html',
})
export class ResidentFormFieldComponent {
  @Input() mode = 'single';
  @Input() submitted = false;
  @Input() control;
  @Input() label;
  @Input() required = false;
  @Input() appAutofocus = false;
  @Input() serverSideSearch = false;
  @Input() fetchFn = null;
  @Input() mapperFn = null;
  @Input()
  ngForm: any;
  @Input() showCreate = false;

  constructor(
    public service: ResidentFormModalService,
    public residentService: ResidentService,
  ) {}

  public get hasPermissionToCreate() {
    return this.residentService.hasPermissionToCreate;
  }

  public async createModalOpen() {
    const record = await this.service.open();
    if (record) {
      if (this.mode === 'multiple') {
        this.control.setValue([
          ...(this.control.value || []),
          this.mapperFn(record),
        ]);
      } else {
        this.control.setValue(this.mapperFn(record));
      }
    }
  }
}
