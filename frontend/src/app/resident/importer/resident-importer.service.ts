import { Injectable } from '@angular/core';
import { ResidentApi } from 'src/app/resident/resident.api';
import residentImporterFields from 'src/app/resident/importer/resident-importer-fields';
import { ErrorService } from 'src/app/shared/error/error.service';
import { i18n } from 'src/i18n';
import { ImporterService } from 'src/app/shared/importer/importer.service';

@Injectable({
  providedIn: 'root',
})
export class ResidentImporterService extends ImporterService {
  constructor(errorService: ErrorService) {
    super(
      errorService,
      ResidentApi.import,
      residentImporterFields,
      i18n('entities.resident.importer.fileName'),
      i18n('entities.resident.importer.hint'),
    );
  }
}
