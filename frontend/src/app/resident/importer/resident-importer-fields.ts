import { ResidentModel } from 'src/app/resident/resident-model';

const { fields } = ResidentModel;

export default [
  fields.suffix,
  fields.firstName,
  fields.middleName,
  fields.lastName,
  fields.planToVaccinate,
  fields.birthDate,
  fields.region,
  fields.city,
  fields.barangay,
  fields.email,
  fields.mobileNumber,
  fields.landline,
  fields.username,
  fields.password,
];
