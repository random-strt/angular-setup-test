import { Component } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { i18n } from 'src/i18n';

@Component({
  selector: 'app-resident-importer',
  templateUrl: './resident-importer.component.html',
})
export class ResidentImporterComponent {
  constructor(private authService: AuthService) {}

  breadcrumb = [
    [i18n('dashboard.menu'), '/'],
    [i18n('entities.resident.menu'), '/resident'],
    [i18n('entities.resident.importer.title')],
  ];
}
