import { Component, OnInit } from '@angular/core';
import { i18n } from 'src/i18n';
import { ResidentViewService } from 'src/app/resident/view/resident-view.service';
import { ResidentService } from 'src/app/resident/resident.service';
import { AuditLogService } from 'src/app/audit-log/audit-log.service';
import { ResidentDestroyService } from 'src/app/resident/destroy/resident-destroy.service';
import { ConfirmService } from 'src/app/shared/confirm/confirm.service';

@Component({
  selector: 'app-resident-view-toolbar',
  templateUrl: './resident-view-toolbar.component.html',
})
export class ResidentViewToolbarComponent {
  constructor(
    public service: ResidentViewService,
    private residentService: ResidentService,
    private destroyService: ResidentDestroyService,
    private auditLogService: AuditLogService,
    private confirmService: ConfirmService,
  ) {}

  async doDestroy() {
    const result = await this.confirmService.confirm();

    if (!result) {
      return;
    }

    return this.destroyService.doDestroy(this.record.id);
  }

  get destroyLoading() {
    return this.destroyService.loading;
  }

  get hasPermissionToDestroy() {
    return this.residentService.hasPermissionToDestroy;
  }

  get hasPermissionToEdit() {
    return this.residentService.hasPermissionToEdit;
  }

  get hasPermissionToImport() {
    return this.residentService.hasPermissionToImport;
  }

  get hasPermissionToAuditLogs() {
    return this.auditLogService.hasPermissionToRead;
  }

  get record() {
    return this.service.record;
  }
}
