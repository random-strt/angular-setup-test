import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ResidentModel } from 'src/app/resident/resident-model';
import { ResidentViewService } from 'src/app/resident/view/resident-view.service';
import { i18n } from 'src/i18n';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-resident-view',
  templateUrl: './resident-view.component.html',
})
export class ResidentViewComponent implements OnInit {
  constructor(
    private service: ResidentViewService,
    private route: ActivatedRoute,
    private authService: AuthService,
  ) {}

  async ngOnInit() {
    await this.service.doFind(
      this.route.snapshot.paramMap.get('id'),
    );
  }

  presenter(row, fieldName) {
    return ResidentModel.presenter(row, fieldName);
  }

  get fields() {
    return ResidentModel.fields;
  }

  get loading() {
    return this.service.loading;
  }

  get record() {
    return this.service.record;
  }

  breadcrumb = [
    [i18n('dashboard.menu'), '/'],
    [i18n('entities.resident.menu'), '/resident'],
    [i18n('entities.resident.view.title')],
  ];
}
