import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { PermissionGuard } from 'src/app/auth/permission.guard';
import { Permissions } from 'src/security/permissions';
import { ResidentListComponent } from 'src/app/resident/list/resident-list.component';
import { ResidentViewComponent } from 'src/app/resident/view/resident-view.component';
import { ResidentImporterComponent } from 'src/app/resident/importer/resident-importer.component';
import { ResidentFormPageComponent } from 'src/app/resident/form/resident-form-page.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: ':id/edit',
        component: ResidentFormPageComponent,
        canActivate: [AuthGuard, PermissionGuard],
        data: {
          permission: Permissions.values.residentEdit,
        },
      },
      {
        path: 'new',
        component: ResidentFormPageComponent,
        canActivate: [AuthGuard, PermissionGuard],
        data: {
          permission: Permissions.values.residentCreate,
        },
      },
      {
        path: 'import',
        component: ResidentImporterComponent,
        canActivate: [AuthGuard, PermissionGuard],
        data: {
          permission: Permissions.values.residentImport,
        },
      },
      {
        path: ':id',
        component: ResidentViewComponent,
        canActivate: [AuthGuard, PermissionGuard],
        data: {
          permission: Permissions.values.residentRead,
        },
      },
      {
        path: '',
        component: ResidentListComponent,
        canActivate: [AuthGuard, PermissionGuard],
        data: {
          permission: Permissions.values.residentRead,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class ResidentRoutingModule {}

export const routedComponents = [
  ResidentListComponent,
  ResidentFormPageComponent,
  ResidentViewComponent,
  ResidentImporterComponent,
];
