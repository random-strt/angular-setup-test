import { Component } from '@angular/core';
import { ResidentListService } from 'src/app/resident/list/resident-list.service';
import { ResidentService } from 'src/app/resident/resident.service';
import { ResidentModel } from 'src/app/resident/resident-model';
import { ConfirmService } from 'src/app/shared/confirm/confirm.service';
import { ResidentDestroyService } from 'src/app/resident/destroy/resident-destroy.service';
import { i18n } from 'src/i18n';

@Component({
  selector: 'app-resident-list-table',
  templateUrl: './resident-list-table.component.html',
})
export class ResidentListTableComponent {
  constructor(
    public service: ResidentListService,
    public destroyService: ResidentDestroyService,
    public residentService: ResidentService,
    private confirmService: ConfirmService,
  ) {}

  presenter(row, fieldName) {
    return ResidentModel.presenter(row, fieldName);
  }

  i18n(key) {
    return i18n(key);
  }

  async doDestroy(id) {
    const response = await this.confirmService.confirm();

    if (!response) {
      return;
    }

    return this.destroyService.doDestroy(id);
  }

  get hasPermissionToEdit() {
    return this.residentService.hasPermissionToEdit;
  }

  get hasPermissionToDestroy() {
    return this.residentService.hasPermissionToDestroy;
  }

  get fields() {
    return ResidentModel.fields;
  }

  get columns() {
    return [
      '_select',

      this.fields.suffix.name,
      this.fields.firstName.name,
      this.fields.middleName.name,
      this.fields.lastName.name,
      this.fields.planToVaccinate.name,
      this.fields.birthDate.name,
      this.fields.region.name,
      this.fields.city.name,
      this.fields.barangay.name,
      this.fields.email.name,
      this.fields.mobileNumber.name,
      this.fields.landline.name,
      this.fields.username.name,
      this.fields.password.name,

      '_actions',
    ];
  }
}
