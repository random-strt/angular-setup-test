import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FilterSchema } from 'src/app/shared/form/filter-schema';
import { ResidentListService } from 'src/app/resident/list/resident-list.service';
import { ResidentModel } from 'src/app/resident/resident-model';

@Component({
  selector: 'app-resident-list-filter',
  templateUrl: './resident-list-filter.component.html',
})
export class ResidentListFilterComponent implements OnInit {
  form: FormGroup;
  schema: FilterSchema;
  expanded: boolean = false;

  constructor(
    private service: ResidentListService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
  ) {}

  async ngOnInit() {
    this.buildSchema();
    this.buildForm();
    this.doFilter();
  }

  get fields() {
    return ResidentModel.fields;
  }

  get loading() {
    return this.service.loading;
  }

  doRemove(key) {
    this.form.get(key).setValue(null);
    this.expanded = false;
    const values = this.schema.cast(this.form.value);
    return this.service.doFetch(values);
  }

  doToggleExpanded() {
    this.expanded = !this.expanded;
  }

  doFilter() {
    if (!this.form.valid) {
      return;
    }

    this.expanded = false;
    const values = this.schema.cast(this.form.value);
    return this.service.doFetch(values);
  }

  buildForm() {
    const { filter } = this.service;
    const params = this.route.snapshot.queryParams;
    this.form = this.schema.buildForm(filter, params);
  }

  doReset() {
    this.form = this.schema.buildForm();
    this.doFilter();
    this.expanded = false;
  }

  private buildSchema() {
    this.schema = new FilterSchema(
      [
        this.fields.suffix,
        this.fields.firstName,
        this.fields.middleName,
        this.fields.lastName,
        this.fields.planToVaccinate,
        this.fields.birthDateRange,
        this.fields.region,
        this.fields.city,
        this.fields.barangay,
        this.fields.email,
        this.fields.mobileNumber,
        this.fields.landline,
        this.fields.username,
        this.fields.password,
      ],
      this.formBuilder,
    );
  }
}
