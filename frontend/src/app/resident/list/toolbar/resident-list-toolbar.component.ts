import { Component, OnInit } from '@angular/core';
import { i18n } from 'src/i18n';
import { ResidentListService } from 'src/app/resident/list/resident-list.service';
import { ResidentService } from 'src/app/resident/resident.service';
import { AuditLogService } from 'src/app/audit-log/audit-log.service';
import { ConfirmService } from 'src/app/shared/confirm/confirm.service';
import { ResidentDestroyService } from 'src/app/resident/destroy/resident-destroy.service';

@Component({
  selector: 'app-resident-list-toolbar',
  templateUrl: './resident-list-toolbar.component.html',
})
export class ResidentListToolbarComponent {
  constructor(
    public service: ResidentListService,
    private residentService: ResidentService,
    private destroyService: ResidentDestroyService,
    private auditLogService: AuditLogService,
    private confirmService: ConfirmService,
  ) {}

  get destroyButtonDisabled() {
    return (
      this.service.selectedKeys.isEmpty() ||
      this.service.loading ||
      this.destroyService.loading
    );
  }

  get destroyButtonTooltip() {
    if (
      this.service.selectedKeys.isEmpty() ||
      this.service.loading
    ) {
      return i18n('common.mustSelectARow');
    }
  }

  async doDestroyAllSelected() {
    const result = await this.confirmService.confirm();

    if (!result) {
      return;
    }

    return this.destroyService.doDestroyAll(
      this.service.selectedKeys.selected,
    );
  }

  get hasPermissionToAuditLogs() {
    return this.auditLogService.hasPermissionToRead;
  }

  get hasPermissionToCreate() {
    return this.residentService.hasPermissionToCreate;
  }

  get hasPermissionToDestroy() {
    return this.residentService.hasPermissionToDestroy;
  }

  get hasPermissionToEdit() {
    return this.residentService.hasPermissionToEdit;
  }

  get hasPermissionToImport() {
    return this.residentService.hasPermissionToImport;
  }

  doExport() {
    return this.service.doExport();
  }

  get exportButtonDisabled() {
    return (
      !this.service.hasRows ||
      this.service.loading ||
      this.service.exportLoading
    );
  }

  get exportButtonTooltip() {
    if (!this.service.hasRows || this.service.loading) {
      return i18n('common.noDataToExport');
    }
  }
}
