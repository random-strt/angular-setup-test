import {
  Component,
  OnInit,
  Output,
  Input,
  EventEmitter,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ResidentModel } from 'src/app/resident/resident-model';
import { FormSchema } from 'src/app/shared/form/form-schema';

@Component({
  selector: 'app-resident-form',
  templateUrl: './resident-form.component.html',
})
export class ResidentFormComponent implements OnInit {
  form: FormGroup;
  schema: FormSchema;

  @Output() save = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Input() isEditing;
  @Input() record;
  @Input() saveLoading;
  @Input() modal = false;

  constructor(private formBuilder: FormBuilder) {}

  async ngOnInit() {
    this.buildSchema();
    this.buildForm();
  }

  get fields() {
    return ResidentModel.fields;
  }

  doSave() {
    if (!this.form.valid) {
      return;
    }

    const id = this.record && this.record.id;
    const values = this.schema.cast(this.form.value);

    this.save.emit({ id, values });
  }

  doReset() {
    this.buildForm();
  }

  buildForm() {
    this.form = this.schema.buildForm(this.record);
  }

  private buildSchema() {
    this.schema = new FormSchema(
      [
        this.fields.suffix,
        this.fields.firstName,
        this.fields.middleName,
        this.fields.lastName,
        this.fields.planToVaccinate,
        this.fields.birthDate,
        this.fields.region,
        this.fields.city,
        this.fields.barangay,
        this.fields.email,
        this.fields.mobileNumber,
        this.fields.landline,
        this.fields.username,
        this.fields.password,
      ],
      this.formBuilder,
    );
  }
}
