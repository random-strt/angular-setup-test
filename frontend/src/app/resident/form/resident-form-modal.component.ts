import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Snackbar } from 'src/app/shared/snackbar/snackbar.service';
import { ErrorService } from 'src/app/shared/error/error.service';
import { ResidentApi } from 'src/app/resident/resident.api';
import { i18n } from 'src/i18n';

@Component({
  selector: 'app-resident-form-modal',
  templateUrl: './resident-form-modal.component.html',
})
export class ResidentFormModalComponent {
  saveLoading = false;

  constructor(
    public dialogRef: MatDialogRef<
      ResidentFormModalComponent
    >,
    private errorService: ErrorService,
    private snackbar: Snackbar,
  ) {}

  async doCreate({ id, values }) {
    try {
      this.saveLoading = true;
      const { id } = await ResidentApi.create(values);
      const record = await ResidentApi.find(id);
      this.saveLoading = false;

      this.snackbar.success(
        i18n('entities.resident.create.success'),
      );

      if (this.dialogRef) {
        this.dialogRef.close(record);
      }
    } catch (error) {
      this.errorService.handle(error);
      this.saveLoading = false;
    }
  }

  doCancel() {
    this.dialogRef.close();
  }
}
