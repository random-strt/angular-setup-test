import { Injectable } from '@angular/core';
import { Snackbar } from 'src/app/shared/snackbar/snackbar.service';
import { ErrorService } from 'src/app/shared/error/error.service';
import { ResidentApi } from 'src/app/resident/resident.api';
import { ResidentListService } from 'src/app/resident/list/resident-list.service';
import { Router } from '@angular/router';
import { i18n } from 'src/i18n';

@Injectable({
  providedIn: 'root',
})
export class ResidentDestroyService {
  loading = false;

  constructor(
    private errorService: ErrorService,
    private snackbar: Snackbar,
    private router: Router,
    private residentListService: ResidentListService,
  ) {}

  async doDestroy(id) {
    try {
      this.loading = true;
      await ResidentApi.destroyAll([id]);
      this.loading = false;
      this.snackbar.success(
        i18n('entities.resident.destroy.success'),
      );

      this.router.navigate(['/resident']);

      await this.residentListService.doFetch(
        this.residentListService.filter,
      );
    } catch (error) {
      this.errorService.handle(error);
      this.loading = false;
    }
  }

  async doDestroyAll(ids) {
    try {
      this.loading = true;
      await ResidentApi.destroyAll(ids);
      this.loading = false;

      this.residentListService.doResetSelectedKeys();

      this.snackbar.success(
        i18n('entities.resident.destroyAll.success'),
      );

      this.router.navigate(['/resident']);

      return this.residentListService.doFetch(
        this.residentListService.filter,
      );
    } catch (error) {
      this.errorService.handle(error);
      this.loading = false;
    }
  }
}
