import { NgModule } from '@angular/core';
import { LayoutModule } from 'src/app/layout/layout.module';
import {
  ResidentRoutingModule,
  routedComponents,
} from 'src/app/resident/resident-routing.module';
import { ResidentListFilterComponent } from 'src/app/resident/list/filter/resident-list-filter.component';
import { ResidentListTableComponent } from 'src/app/resident/list/table/resident-list-table.component';
import { ResidentListToolbarComponent } from 'src/app/resident/list/toolbar/resident-list-toolbar.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ResidentViewToolbarComponent } from 'src/app/resident/view/resident-view-toolbar.component';
import { ImporterService } from 'src/app/shared/importer/importer.service';
import { ResidentImporterService } from 'src/app/resident/importer/resident-importer.service';
import { AppFormAutocompleteModule } from 'src/app/app-form-autocomplete.module';

@NgModule({
  declarations: [
    ...routedComponents,
    ResidentListFilterComponent,
    ResidentListTableComponent,
    ResidentListToolbarComponent,
    ResidentViewToolbarComponent,
  ],
  imports: [
    SharedModule,
    ResidentRoutingModule,
    LayoutModule,
    AppFormAutocompleteModule,
  ],
  exports: [],
  providers: [
    {
      provide: ImporterService,
      useClass: ResidentImporterService,
    },
  ],
})
export class ResidentModule {}
