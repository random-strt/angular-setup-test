import RelationToOneField from 'src/app/shared/fields/relation-to-one-field';
import { ResidentApi } from 'src/app/resident/resident.api';
import { Permissions } from 'src/security/permissions';
import RelationToManyField from 'src/app/shared/fields/relation-to-many-field';

export class ResidentField {
  static relationToOne(name, label, options?) {
    return new RelationToOneField(
      name,
      label,
      '/resident',
      Permissions.values.residentRead,
      ResidentApi.listAutocomplete,
      (record) => {
        if (!record) {
          return null;
        }

        return {
          id: record.id,
          label: record.id,
        };
      },
      options,
    );
  }

  static relationToMany(name, label, options?) {
    return new RelationToManyField(
      name,
      label,
      '/resident',
      Permissions.values.residentRead,
      ResidentApi.listAutocomplete,
      (record) => {
        if (!record) {
          return null;
        }

        return {
          id: record.id,
          label: record.id,
        };
      },
      options,
    );
  }
}
