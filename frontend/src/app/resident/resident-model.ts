import { GenericModel } from 'src/app/shared/model/generic-model';
import { i18n } from 'src/i18n';
import DateTimeField from 'src/app/shared/fields/date-time-field';
import IdField from 'src/app/shared/fields/id-field';
import DateTimeRangeField from 'src/app/shared/fields/date-time-range-field';
import StringField from 'src/app/shared/fields/string-field';
import BooleanField from 'src/app/shared/fields/boolean-field';
import DateField from 'src/app/shared/fields/date-field';
import DateRangeField from 'src/app/shared/fields/date-range-field';

function label(name) {
  return i18n(`entities.resident.fields.${name}`);
}

const fields = {
  id: new IdField('id', label('id')),
  suffix: new StringField('suffix', label('suffix'), {}),
  firstName: new StringField('firstName', label('firstName'), {}),
  middleName: new StringField('middleName', label('middleName'), {}),
  lastName: new StringField('lastName', label('lastName'), {}),
  planToVaccinate: new BooleanField('planToVaccinate', label('planToVaccinate')),
  birthDate: new DateField('birthDate', label('birthDate'), {}),
  region: new StringField('region', label('region'), {}),
  city: new StringField('city', label('city'), {}),
  barangay: new StringField('barangay', label('barangay'), {}),
  email: new StringField('email', label('email'), {}),
  mobileNumber: new StringField('mobileNumber', label('mobileNumber'), {}),
  landline: new StringField('landline', label('landline'), {}),
  username: new StringField('username', label('username'), {}),
  password: new StringField('password', label('password'), {}),
  createdAt: new DateTimeField(
    'createdAt',
    label('createdAt'),
  ),
  updatedAt: new DateTimeField(
    'updatedAt',
    label('updatedAt'),
  ),
  createdAtRange: new DateTimeRangeField(
    'createdAtRange',
    label('createdAtRange'),
  ),
  birthDateRange: new DateRangeField(
    'birthDateRange',
    label('birthDateRange'),
  ),
};

export class ResidentModel extends GenericModel {
  static get fields() {
    return fields;
  }
}
