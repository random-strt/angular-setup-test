import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { PermissionGuard } from 'src/app/auth/permission.guard';
import { Permissions } from 'src/security/permissions';
import { BusinessListComponent } from 'src/app/business/list/business-list.component';
import { BusinessViewComponent } from 'src/app/business/view/business-view.component';
import { BusinessImporterComponent } from 'src/app/business/importer/business-importer.component';
import { BusinessFormPageComponent } from 'src/app/business/form/business-form-page.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: ':id/edit',
        component: BusinessFormPageComponent,
        canActivate: [AuthGuard, PermissionGuard],
        data: {
          permission: Permissions.values.businessEdit,
        },
      },
      {
        path: 'new',
        component: BusinessFormPageComponent,
        canActivate: [AuthGuard, PermissionGuard],
        data: {
          permission: Permissions.values.businessCreate,
        },
      },
      {
        path: 'import',
        component: BusinessImporterComponent,
        canActivate: [AuthGuard, PermissionGuard],
        data: {
          permission: Permissions.values.businessImport,
        },
      },
      {
        path: ':id',
        component: BusinessViewComponent,
        canActivate: [AuthGuard, PermissionGuard],
        data: {
          permission: Permissions.values.businessRead,
        },
      },
      {
        path: '',
        component: BusinessListComponent,
        canActivate: [AuthGuard, PermissionGuard],
        data: {
          permission: Permissions.values.businessRead,
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class BusinessRoutingModule {}

export const routedComponents = [
  BusinessListComponent,
  BusinessFormPageComponent,
  BusinessViewComponent,
  BusinessImporterComponent,
];
