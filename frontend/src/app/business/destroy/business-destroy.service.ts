import { Injectable } from '@angular/core';
import { Snackbar } from 'src/app/shared/snackbar/snackbar.service';
import { ErrorService } from 'src/app/shared/error/error.service';
import { BusinessApi } from 'src/app/business/business.api';
import { BusinessListService } from 'src/app/business/list/business-list.service';
import { Router } from '@angular/router';
import { i18n } from 'src/i18n';

@Injectable({
  providedIn: 'root',
})
export class BusinessDestroyService {
  loading = false;

  constructor(
    private errorService: ErrorService,
    private snackbar: Snackbar,
    private router: Router,
    private businessListService: BusinessListService,
  ) {}

  async doDestroy(id) {
    try {
      this.loading = true;
      await BusinessApi.destroyAll([id]);
      this.loading = false;
      this.snackbar.success(
        i18n('entities.business.destroy.success'),
      );

      this.router.navigate(['/business']);

      await this.businessListService.doFetch(
        this.businessListService.filter,
      );
    } catch (error) {
      this.errorService.handle(error);
      this.loading = false;
    }
  }

  async doDestroyAll(ids) {
    try {
      this.loading = true;
      await BusinessApi.destroyAll(ids);
      this.loading = false;

      this.businessListService.doResetSelectedKeys();

      this.snackbar.success(
        i18n('entities.business.destroyAll.success'),
      );

      this.router.navigate(['/business']);

      return this.businessListService.doFetch(
        this.businessListService.filter,
      );
    } catch (error) {
      this.errorService.handle(error);
      this.loading = false;
    }
  }
}
