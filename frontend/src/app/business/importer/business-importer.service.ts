import { Injectable } from '@angular/core';
import { BusinessApi } from 'src/app/business/business.api';
import businessImporterFields from 'src/app/business/importer/business-importer-fields';
import { ErrorService } from 'src/app/shared/error/error.service';
import { i18n } from 'src/i18n';
import { ImporterService } from 'src/app/shared/importer/importer.service';

@Injectable({
  providedIn: 'root',
})
export class BusinessImporterService extends ImporterService {
  constructor(errorService: ErrorService) {
    super(
      errorService,
      BusinessApi.import,
      businessImporterFields,
      i18n('entities.business.importer.fileName'),
      i18n('entities.business.importer.hint'),
    );
  }
}
