import { Component } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { i18n } from 'src/i18n';

@Component({
  selector: 'app-business-importer',
  templateUrl: './business-importer.component.html',
})
export class BusinessImporterComponent {
  constructor(private authService: AuthService) {}

  breadcrumb = [
    [i18n('dashboard.menu'), '/'],
    [i18n('entities.business.menu'), '/business'],
    [i18n('entities.business.importer.title')],
  ];
}
