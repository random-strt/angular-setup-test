import { BusinessModel } from 'src/app/business/business-model';

const { fields } = BusinessModel;

export default [
  fields.contactPerson,
  fields.email,
  fields.mobileNumber,
  fields.landlineNumber,
  fields.password,
];
