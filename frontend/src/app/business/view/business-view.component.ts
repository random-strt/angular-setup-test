import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BusinessModel } from 'src/app/business/business-model';
import { BusinessViewService } from 'src/app/business/view/business-view.service';
import { i18n } from 'src/i18n';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-business-view',
  templateUrl: './business-view.component.html',
})
export class BusinessViewComponent implements OnInit {
  constructor(
    private service: BusinessViewService,
    private route: ActivatedRoute,
    private authService: AuthService,
  ) {}

  async ngOnInit() {
    await this.service.doFind(
      this.route.snapshot.paramMap.get('id'),
    );
  }

  presenter(row, fieldName) {
    return BusinessModel.presenter(row, fieldName);
  }

  get fields() {
    return BusinessModel.fields;
  }

  get loading() {
    return this.service.loading;
  }

  get record() {
    return this.service.record;
  }

  breadcrumb = [
    [i18n('dashboard.menu'), '/'],
    [i18n('entities.business.menu'), '/business'],
    [i18n('entities.business.view.title')],
  ];
}
