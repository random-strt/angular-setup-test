import { Component, OnInit } from '@angular/core';
import { i18n } from 'src/i18n';
import { BusinessViewService } from 'src/app/business/view/business-view.service';
import { BusinessService } from 'src/app/business/business.service';
import { AuditLogService } from 'src/app/audit-log/audit-log.service';
import { BusinessDestroyService } from 'src/app/business/destroy/business-destroy.service';
import { ConfirmService } from 'src/app/shared/confirm/confirm.service';

@Component({
  selector: 'app-business-view-toolbar',
  templateUrl: './business-view-toolbar.component.html',
})
export class BusinessViewToolbarComponent {
  constructor(
    public service: BusinessViewService,
    private businessService: BusinessService,
    private destroyService: BusinessDestroyService,
    private auditLogService: AuditLogService,
    private confirmService: ConfirmService,
  ) {}

  async doDestroy() {
    const result = await this.confirmService.confirm();

    if (!result) {
      return;
    }

    return this.destroyService.doDestroy(this.record.id);
  }

  get destroyLoading() {
    return this.destroyService.loading;
  }

  get hasPermissionToDestroy() {
    return this.businessService.hasPermissionToDestroy;
  }

  get hasPermissionToEdit() {
    return this.businessService.hasPermissionToEdit;
  }

  get hasPermissionToImport() {
    return this.businessService.hasPermissionToImport;
  }

  get hasPermissionToAuditLogs() {
    return this.auditLogService.hasPermissionToRead;
  }

  get record() {
    return this.service.record;
  }
}
