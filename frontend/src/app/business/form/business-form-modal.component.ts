import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Snackbar } from 'src/app/shared/snackbar/snackbar.service';
import { ErrorService } from 'src/app/shared/error/error.service';
import { BusinessApi } from 'src/app/business/business.api';
import { i18n } from 'src/i18n';

@Component({
  selector: 'app-business-form-modal',
  templateUrl: './business-form-modal.component.html',
})
export class BusinessFormModalComponent {
  saveLoading = false;

  constructor(
    public dialogRef: MatDialogRef<
      BusinessFormModalComponent
    >,
    private errorService: ErrorService,
    private snackbar: Snackbar,
  ) {}

  async doCreate({ id, values }) {
    try {
      this.saveLoading = true;
      const { id } = await BusinessApi.create(values);
      const record = await BusinessApi.find(id);
      this.saveLoading = false;

      this.snackbar.success(
        i18n('entities.business.create.success'),
      );

      if (this.dialogRef) {
        this.dialogRef.close(record);
      }
    } catch (error) {
      this.errorService.handle(error);
      this.saveLoading = false;
    }
  }

  doCancel() {
    this.dialogRef.close();
  }
}
