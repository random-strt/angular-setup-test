import RelationToOneField from 'src/app/shared/fields/relation-to-one-field';
import { BusinessApi } from 'src/app/business/business.api';
import { Permissions } from 'src/security/permissions';
import RelationToManyField from 'src/app/shared/fields/relation-to-many-field';

export class BusinessField {
  static relationToOne(name, label, options?) {
    return new RelationToOneField(
      name,
      label,
      '/business',
      Permissions.values.businessRead,
      BusinessApi.listAutocomplete,
      (record) => {
        if (!record) {
          return null;
        }

        return {
          id: record.id,
          label: record.id,
        };
      },
      options,
    );
  }

  static relationToMany(name, label, options?) {
    return new RelationToManyField(
      name,
      label,
      '/business',
      Permissions.values.businessRead,
      BusinessApi.listAutocomplete,
      (record) => {
        if (!record) {
          return null;
        }

        return {
          id: record.id,
          label: record.id,
        };
      },
      options,
    );
  }
}
