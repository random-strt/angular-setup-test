import { NgModule } from '@angular/core';
import { LayoutModule } from 'src/app/layout/layout.module';
import {
  BusinessRoutingModule,
  routedComponents,
} from 'src/app/business/business-routing.module';
import { BusinessListFilterComponent } from 'src/app/business/list/filter/business-list-filter.component';
import { BusinessListTableComponent } from 'src/app/business/list/table/business-list-table.component';
import { BusinessListToolbarComponent } from 'src/app/business/list/toolbar/business-list-toolbar.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BusinessViewToolbarComponent } from 'src/app/business/view/business-view-toolbar.component';
import { ImporterService } from 'src/app/shared/importer/importer.service';
import { BusinessImporterService } from 'src/app/business/importer/business-importer.service';
import { AppFormAutocompleteModule } from 'src/app/app-form-autocomplete.module';

@NgModule({
  declarations: [
    ...routedComponents,
    BusinessListFilterComponent,
    BusinessListTableComponent,
    BusinessListToolbarComponent,
    BusinessViewToolbarComponent,
  ],
  imports: [
    SharedModule,
    BusinessRoutingModule,
    LayoutModule,
    AppFormAutocompleteModule,
  ],
  exports: [],
  providers: [
    {
      provide: ImporterService,
      useClass: BusinessImporterService,
    },
  ],
})
export class BusinessModule {}
