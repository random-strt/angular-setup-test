import { GenericModel } from 'src/app/shared/model/generic-model';
import { i18n } from 'src/i18n';
import DateTimeField from 'src/app/shared/fields/date-time-field';
import IdField from 'src/app/shared/fields/id-field';
import DateTimeRangeField from 'src/app/shared/fields/date-time-range-field';
import StringField from 'src/app/shared/fields/string-field';

function label(name) {
  return i18n(`entities.business.fields.${name}`);
}

const fields = {
  id: new IdField('id', label('id')),
  contactPerson: new StringField('contactPerson', label('contactPerson'), {}),
  email: new StringField('email', label('email'), {}),
  mobileNumber: new StringField('mobileNumber', label('mobileNumber'), {}),
  landlineNumber: new StringField('landlineNumber', label('landlineNumber'), {}),
  password: new StringField('password', label('password'), {}),
  createdAt: new DateTimeField(
    'createdAt',
    label('createdAt'),
  ),
  updatedAt: new DateTimeField(
    'updatedAt',
    label('updatedAt'),
  ),
  createdAtRange: new DateTimeRangeField(
    'createdAtRange',
    label('createdAtRange'),
  ),

};

export class BusinessModel extends GenericModel {
  static get fields() {
    return fields;
  }
}
