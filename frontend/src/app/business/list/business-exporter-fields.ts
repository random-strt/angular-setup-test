import { BusinessModel } from 'src/app/business/business-model';

const { fields } = BusinessModel;

export default [
  fields.id,
  fields.contactPerson,
  fields.email,
  fields.mobileNumber,
  fields.landlineNumber,
  fields.password,
  fields.createdAt,
  fields.updatedAt,
];
