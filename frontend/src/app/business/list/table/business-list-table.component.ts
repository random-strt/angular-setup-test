import { Component } from '@angular/core';
import { BusinessListService } from 'src/app/business/list/business-list.service';
import { BusinessService } from 'src/app/business/business.service';
import { BusinessModel } from 'src/app/business/business-model';
import { ConfirmService } from 'src/app/shared/confirm/confirm.service';
import { BusinessDestroyService } from 'src/app/business/destroy/business-destroy.service';
import { i18n } from 'src/i18n';

@Component({
  selector: 'app-business-list-table',
  templateUrl: './business-list-table.component.html',
})
export class BusinessListTableComponent {
  constructor(
    public service: BusinessListService,
    public destroyService: BusinessDestroyService,
    public businessService: BusinessService,
    private confirmService: ConfirmService,
  ) {}

  presenter(row, fieldName) {
    return BusinessModel.presenter(row, fieldName);
  }

  i18n(key) {
    return i18n(key);
  }

  async doDestroy(id) {
    const response = await this.confirmService.confirm();

    if (!response) {
      return;
    }

    return this.destroyService.doDestroy(id);
  }

  get hasPermissionToEdit() {
    return this.businessService.hasPermissionToEdit;
  }

  get hasPermissionToDestroy() {
    return this.businessService.hasPermissionToDestroy;
  }

  get fields() {
    return BusinessModel.fields;
  }

  get columns() {
    return [
      '_select',

      this.fields.contactPerson.name,
      this.fields.email.name,
      this.fields.mobileNumber.name,
      this.fields.landlineNumber.name,
      this.fields.password.name,

      '_actions',
    ];
  }
}
