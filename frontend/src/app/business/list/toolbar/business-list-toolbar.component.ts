import { Component, OnInit } from '@angular/core';
import { i18n } from 'src/i18n';
import { BusinessListService } from 'src/app/business/list/business-list.service';
import { BusinessService } from 'src/app/business/business.service';
import { AuditLogService } from 'src/app/audit-log/audit-log.service';
import { ConfirmService } from 'src/app/shared/confirm/confirm.service';
import { BusinessDestroyService } from 'src/app/business/destroy/business-destroy.service';

@Component({
  selector: 'app-business-list-toolbar',
  templateUrl: './business-list-toolbar.component.html',
})
export class BusinessListToolbarComponent {
  constructor(
    public service: BusinessListService,
    private businessService: BusinessService,
    private destroyService: BusinessDestroyService,
    private auditLogService: AuditLogService,
    private confirmService: ConfirmService,
  ) {}

  get destroyButtonDisabled() {
    return (
      this.service.selectedKeys.isEmpty() ||
      this.service.loading ||
      this.destroyService.loading
    );
  }

  get destroyButtonTooltip() {
    if (
      this.service.selectedKeys.isEmpty() ||
      this.service.loading
    ) {
      return i18n('common.mustSelectARow');
    }
  }

  async doDestroyAllSelected() {
    const result = await this.confirmService.confirm();

    if (!result) {
      return;
    }

    return this.destroyService.doDestroyAll(
      this.service.selectedKeys.selected,
    );
  }

  get hasPermissionToAuditLogs() {
    return this.auditLogService.hasPermissionToRead;
  }

  get hasPermissionToCreate() {
    return this.businessService.hasPermissionToCreate;
  }

  get hasPermissionToDestroy() {
    return this.businessService.hasPermissionToDestroy;
  }

  get hasPermissionToEdit() {
    return this.businessService.hasPermissionToEdit;
  }

  get hasPermissionToImport() {
    return this.businessService.hasPermissionToImport;
  }

  doExport() {
    return this.service.doExport();
  }

  get exportButtonDisabled() {
    return (
      !this.service.hasRows ||
      this.service.loading ||
      this.service.exportLoading
    );
  }

  get exportButtonTooltip() {
    if (!this.service.hasRows || this.service.loading) {
      return i18n('common.noDataToExport');
    }
  }
}
