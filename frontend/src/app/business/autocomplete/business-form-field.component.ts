import { Component, Input } from '@angular/core';
import { BusinessFormModalService } from 'src/app/business/form/business-form-modal.service';
import { BusinessService } from 'src/app/business/business.service';

@Component({
  selector: 'app-business-form-field',
  templateUrl: './business-form-field.component.html',
})
export class BusinessFormFieldComponent {
  @Input() mode = 'single';
  @Input() submitted = false;
  @Input() control;
  @Input() label;
  @Input() required = false;
  @Input() appAutofocus = false;
  @Input() serverSideSearch = false;
  @Input() fetchFn = null;
  @Input() mapperFn = null;
  @Input()
  ngForm: any;
  @Input() showCreate = false;

  constructor(
    public service: BusinessFormModalService,
    public businessService: BusinessService,
  ) {}

  public get hasPermissionToCreate() {
    return this.businessService.hasPermissionToCreate;
  }

  public async createModalOpen() {
    const record = await this.service.open();
    if (record) {
      if (this.mode === 'multiple') {
        this.control.setValue([
          ...(this.control.value || []),
          this.mapperFn(record),
        ]);
      } else {
        this.control.setValue(this.mapperFn(record));
      }
    }
  }
}
