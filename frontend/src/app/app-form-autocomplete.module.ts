import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';

import { TenantFormComponent } from 'src/app/tenant/form/tenant-form.component';

import { UserNewFormComponent } from 'src/app/user/form/user-new-form.component';
import { UserNewFormModalComponent } from 'src/app/user/form/user-new-form-modal.component';
import { UserNewFormModalService } from 'src/app/user/form/user-new-form-modal.service';
import { UserFormFieldComponent } from 'src/app/user/autocomplete/user-form-field.component';

import { ResidentFormFieldComponent } from 'src/app/resident/autocomplete/resident-form-field.component';
import { ResidentFormModalComponent } from 'src/app/resident/form/resident-form-modal.component';
import { ResidentFormModalService } from 'src/app/resident/form/resident-form-modal.service';
import { ResidentFormComponent } from 'src/app/resident/form/resident-form.component';

import { BusinessFormFieldComponent } from 'src/app/business/autocomplete/business-form-field.component';
import { BusinessFormModalComponent } from 'src/app/business/form/business-form-modal.component';
import { BusinessFormModalService } from 'src/app/business/form/business-form-modal.service';
import { BusinessFormComponent } from 'src/app/business/form/business-form.component';

/**
 * This module exists to avoid circular dependencies, because autocompletes and forms
 * from different modules may use each others.
 */
@NgModule({
  declarations: [
    TenantFormComponent,

    UserNewFormComponent,
    UserFormFieldComponent,
    UserNewFormModalComponent,

    ResidentFormComponent,
    ResidentFormFieldComponent,
    ResidentFormModalComponent,

    BusinessFormComponent,
    BusinessFormFieldComponent,
    BusinessFormModalComponent,
  ],
  imports: [SharedModule],
  exports: [
    TenantFormComponent,

    UserNewFormComponent,
    UserFormFieldComponent,

    ResidentFormComponent,
    ResidentFormFieldComponent,

    BusinessFormComponent,
    BusinessFormFieldComponent,
  ],
  providers: [
    UserNewFormModalService,
    ResidentFormModalService,

    BusinessFormModalService,
  ],
  entryComponents: [
    UserNewFormModalComponent,
    ResidentFormModalComponent,

    BusinessFormModalComponent,
  ],
})
export class AppFormAutocompleteModule {}
